(ns dailycodingproblem.subseq-test
  (:require [clojure.test :refer :all]
            [dailycodingproblem.subseq :refer :all]))

(deftest a-test
  (testing "example problem from https://www.dailycodingproblem.com/blog/longest-increasing-subsequence/"
    (let [test-vec [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]]
      (is (= [0, 2, 6, 9, 11, 15]
             (longest-subseq test-vec))))))
