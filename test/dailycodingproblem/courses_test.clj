(ns dailycodingproblem.courses-test
  (:require [clojure.test :refer :all]
            [dailycodingproblem.courses :refer :all]))

(def sample1
  {:CSC300 [:CSC200 :CSC100]
   :CSC200 [:CSC100]
   :CSC100 []})

(deftest sample-test
  (testing "Provided sample should pass"
    (is (= (order-courses sample1)
           [:CSC100 :CSC200 :CSC300]))))
