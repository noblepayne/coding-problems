(ns dailycodingproblem.subseq)

(defn longest-subseq
  ([v] (longest-subseq v '()))
  ([v subseq]))

;; :curr [building-list stock-list] :stack [[b s] [b s] [b s]] :output [l l l l]
(defn subseqs
  ([s] (subseqs [nil s] '() [] ))
  ([[b s :as curr] [f & r :as stack] output]
   (println b s f r)
   (cond
     (and b s)     (recur
                    [(conj b (first s)) (next s)]
                    (-> stack (conj [b (next s)]) (conj [nil s]))
                    output)
     s             (recur
                    [(first s) (next s)]
                    (conj stack [nil s])
                    output)
     b             (recur
                    f
                    r
                    (conj output b))
     :else         output)))
