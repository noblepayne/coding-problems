(ns dailycodingproblem.courses
  (:require [clojure.set :as set]))


;; plan of attack
;; build link map which is inverse of course map, e.g.
;; its a map of all courses whose values are a list of courses you can take after them

;; pick starting set as elements of course-map that have no pre-reqs
;; then, using link-map, do a breadth first traversal of all connected courses.
;; if this list contains all the keys of course-map its valid ortherwise return nil

(def sample-1
  {:CSC300 [:CSC200 :CSC100]
   :CSC200 [:CSC100]
   :CSC100 []})

(def sample-2
  {:A1 []
   :B1 []
   :A2 [:A1 :B1]
   :B2 [:B1]
   :C1 [:B2 :A2 :B1 :A1]
   :A3 [:A2 :A1 :B1]})

(def sample-3
  {:A1 []
   :B1 []
   :A2 [:A1 :B1]
   :C1 [:B2]})


(defn clean-deps
  "Provided samples include recursive requirements, e.g. CSC300 requires
  CSC200 and CSC100 and CSC200 requires CSC100. clean-deps removes recursive
  requirements to simplify the graph.
  e.g.
  Input:  {:CSC300 [:CSC200 :CSC100], :CSC200 [:CSC100], :CSC100 []}
  output: {:CSC300 #{:CSC200}, :CSC200 #{:CSC100}, :CSC100 #{}}"
  [course-map]
  (->> course-map
       (map
        (fn [[course deps]]
          (let [recursive-deps (mapcat #(get course-map %) deps)
                recursive-dep-set (into #{} recursive-deps)
                depset (into #{} deps)]
            [course (set/difference depset recursive-dep-set)])))
       (into {})))


(defn build-link-map
  "Invert requirements map s.t. mapped value for each course
  is any reachable courses.
  e.g.
  Input:  {:CSC300 #{:CSC200}, :CSC200 #{:CSC100}, :CSC100 #{}}
  Output: {:CSC200 #{:CSC300}, :CSC100 #{:CSC200}}"
  [course-map]
  (reduce (fn [link-map [course deps]]
            (reduce (fn [xs x]
                      (let [cur (get xs x #{})
                            new (conj cur course)]
                        (assoc xs x new)))
                    link-map
                    deps))
          {}
          course-map))

(defn starting-courses
  "Find any courses with no requirements."
  [course-map]
  (->> course-map
       (filter (fn [[k v]]
                 (empty? v)))
       (map first)))

(defn breadth-first-walk
  "Model courses as possibly several disjoint trees. Perform breadth first
  traversal from all starting nodes. Only walk through nodes not already visited."
  ([link-map starts] (breadth-first-walk link-map starts #{} #{} []))
  ([link-map starts next-level visited path]
   (cond
     (and (seq next-level)
          (empty? starts))
     (recur link-map next-level #{} visited path)  ;; finished this level, swap to next

     (empty? starts) path                          ;; finished exploring, return path
     :else
     (let [curr (first starts)
           new-starts (rest starts)
           new-visited (conj visited curr)]
       (if (= visited new-visited)                            ;; otherwise check if we have visited already
         (recur link-map new-starts next-level visited path)  ;; if so just move on to next node
         (recur link-map                                      ;; if not add to path and add its connected nodes to next level
                new-starts
                (into next-level (get link-map curr []))
                new-visited
                (conj path curr)))))))

(defn order-courses
  "Attempt to find a complete traversal of course catalogue.
  If possible returns that path, otherwise nil."
  [course-map]
  (let [course-map (clean-deps course-map)
        link-map   (build-link-map course-map)
        course-set (into #{} (keys course-map))
        starts     (starting-courses course-map)
        path       (breadth-first-walk link-map starts)
        path-set   (into #{} path)]
    (if (= path-set course-set)
      path)))
